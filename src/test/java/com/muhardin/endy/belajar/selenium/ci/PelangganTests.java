package com.muhardin.endy.belajar.selenium.ci;

import com.github.javafaker.Faker;
import java.net.URL;
import java.text.SimpleDateFormat;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PelangganTests {
    
    private static final String URL_SELENIUM_HUB = "http://selenium__standalone-chrome:4444/wd/hub";
    private static final String URL_APLIKASI = "http://training-selenium.herokuapp.com/pelanggan";
    private static final String PESAN_SUKSES = "Data pelanggan berhasil disimpan";
    private static final String PESAN_GAGAL_HARUS_DIISI = "must not be empty";
    private static final String PESAN_GAGAL_FORMAT_EMAIL = "must be a well-formed email address";
    
    private static final String XPATH_TOMBOL_SIMPAN = "/html/body/form/table/tbody/tr[3]/td[2]/input";
    private static final String XPATH_PESAN_ERROR_NAMA = "/html/body/form/table/tbody/tr[1]/td[3]/ul/li";
    private static final String XPATH_PESAN_ERROR_EMAIL = "/html/body/form/table/tbody/tr[2]/td[3]/ul/li";
    
    private static WebDriver webDriver;
    private Faker faker = new Faker();
    
    @BeforeClass
    public static void jalankanBrowser() throws Exception {
        DesiredCapabilities caps = DesiredCapabilities.chrome();
        
        webDriver = new RemoteWebDriver(new URL(URL_SELENIUM_HUB), caps);
        
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test
    public void testSimpanNormal(){
        webDriver.get(URL_APLIKASI+"/form");
        
        // membuat page object untuk form input pelanggan
        FormEditPelanggan form = new FormEditPelanggan(webDriver);
        
        // isi form
        form.isiNama(faker.name().fullName());
        form.isiEmail(faker.name().username()+"@"+faker.internet().domainName());
        form.isiTanggalLahir(new SimpleDateFormat("yyyy-MM-dd").format(faker.date().birthday()));
        form.pilihJenisKelamin(JenisKelamin.WANITA);
        form.pilihPendidikanTerakhir(PendidikanTerakhir.D4);
        form.simpan();
        
        // periksa hasilnya
        Boolean pindahKeHalamanList = (new WebDriverWait(webDriver, 5))
                .until(ExpectedConditions.urlContains("pelanggan/list"));
        
        Assert.assertTrue(pindahKeHalamanList);
        Assert.assertTrue(webDriver.findElement(By.tagName("body"))
                .getText().contains(PESAN_SUKSES));
    }
        
}
